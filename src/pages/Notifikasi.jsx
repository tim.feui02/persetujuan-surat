import React from 'react'
import Navbar from './Navbar';
import { Container, Button, Alert } from 'react-bootstrap'

function Notifikasi() {
    return (
        <>
        <Navbar/>
        <Container className="py-5">
            <h2>Notifikasi</h2>
            <br />
            <Alert variant="primary">
                <div>
                    <b>Dokumen 1</b> telah di-approve!
                    <span className="float-right">
                        <b>09.00</b>
                    </span>
                </div>
                <hr />
                <span>
                    <a href="/detail-dokumen" className="btn btn-sm btn-outline-primary" role="button">Lihat dokumen</a>
                </span>
            </Alert>
            <Alert variant="primary">
                <div>
                    <b>Dokumen 2</b> telah di-approve!
                    <span className="float-right">
                        <b>09.00</b>
                    </span>
                </div>
                <hr />
                <span>
                    <a href="/detail-dokumen" className="btn btn-sm btn-outline-primary" role="button">Lihat dokumen</a>
                </span>
            </Alert>
            <Alert variant="primary">
                <div>
                    <b>Dokumen 3</b> ditolak
                    <span className="float-right">
                        <b>09.00</b>
                    </span>
                </div>
                <hr />
                <span>
                <a href="/detail-dokumen" className="btn btn-sm btn-outline-primary" role="button">Lihat keterangan</a>
                </span>
            </Alert>
        </Container>
        </>
    )
}

export default Notifikasi