import React from 'react'
import { Nav } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome, faBell, faArchive, faUser } from '@fortawesome/free-solid-svg-icons'
import { noAuto } from '@fortawesome/fontawesome-svg-core'

function Navbar() {
    return (
        <Nav variant="pills" className="justify-content-center navbar navbar-expand-xl navbar-dark bg-light py-2 fixed-bottom" defaultActiveKey="/home">
            <Nav.Item className="px-xs-2 px-md-3">
                <NavLink to="home" className="nav-link" href="#">
                    <span className="d-flex justify-content-center">
                        <h5><FontAwesomeIcon icon={faHome} /></h5>
                    </span>
                    <span className="d-flex justify-content-center py-0 d-sm-none d-md-block" style={{ fontWeight:"lighter" }}>
                        Beranda
                    </span>
                </NavLink>
            </Nav.Item>
            <Nav.Item className="px-xs-2 px-md-3">
                <NavLink to="notifikasi" className="nav-link">
                    <span className="d-flex justify-content-center">
                        <h5><FontAwesomeIcon icon={faBell} /></h5>
                    </span>
                    <span className="d-flex justify-content-center py-0" style={{ fontWeight:"lighter" }}>
                        Notifikasi
                    </span>
                </NavLink>
            </Nav.Item>
            <Nav.Item className="px-xs-2 px-md-3">
                <NavLink to="arsip" className="nav-link" href="#">
                    <span className="d-flex justify-content-center">
                        <h5><FontAwesomeIcon icon={faArchive} /></h5>
                    </span>
                    <span className="d-flex justify-content-center py-0" style={{ fontWeight:"lighter" }}>
                        Arsip
                    </span>
                </NavLink>
            </Nav.Item>
            <Nav.Item className="px-xs-2 px-md-3">
                <NavLink to="akun-saya" className="nav-link">
                    <span className="d-flex justify-content-center">
                        <h5><FontAwesomeIcon icon={faUser} /></h5>
                    </span>
                    <span className="d-flex justify-content-center py-0" style={{ fontWeight:"lighter" }}>
                        Akun
                    </span>
                </NavLink>
            </Nav.Item>
        </Nav>
    )
}

export default Navbar