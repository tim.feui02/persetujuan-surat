import React from 'react'
import Navbar from './Navbar';
import { Container, Card, Button, Image, Row, Col, ListGroup } from 'react-bootstrap'
import foto from '../img/profil.png';

function Akun() {
    return (
        <>
        <Navbar/>
        <Container className="py-5">
            <h2>Akun saya</h2>
            <br />
            <Row className="my-4">
                <Col xs={5} md={2}>
                    <Image src={ foto } roundedCircle style={{ width: "100%" }}/>
                </Col>
                <Col xs={7} md={10}>
                    <h5 style={{ lineHeight:"1.5" }}>Nama lengkap user</h5>
                    <h6 style={{ lineHeight:"1.5" }}>Username</h6>
                    <h6 style={{ lineHeight:"1.5" }}>email_user@mail.com</h6>
                    <h6 style={{ lineHeight:"1.5" }}>Jabatan user</h6>
                </Col>
            </Row>
            <ListGroup variant="flush">
                <ListGroup.Item action href="#">
                    Edit profil
                </ListGroup.Item>
                <ListGroup.Item action href="#">
                    Kontak admin
                </ListGroup.Item>
                <ListGroup.Item action href="#">
                    Keluar
                </ListGroup.Item>
            </ListGroup>
        </Container>
        </>
    )
}

export default Akun