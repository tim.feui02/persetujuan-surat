import React from 'react'

class Calender extends React.Component {
    constructor() {
        super()
        
        var today = new Date();
        var month = new Array();
            month[0]="Jan";
            month[1]="Feb";
            month[2]="Mar";
            month[3]="Apr";
            month[4]="May";
            month[5]="Jun";
            month[6]="Jul";
            month[7]="Aug";
            month[8]="Sep";
            month[9]="Oct";
            month[10]="Nov";
            month[11]="Dec";
        var mm = month[today.getMonth()]; //January is 0!
        var dt = today.getDate() + ' ' + mm + ' ' + today.getFullYear()

        this.state = {
            date: dt
        }
    }

    render() {
        return (
            <div className='date'>
                <h2>{this.state.date}</h2>
            </div>
        )
    }
}
export default Calender