import React, { useState, setShow } from 'react'
import Navbar from './Navbar'
import { Container, Button, Table, Badge, Modal, Form } from 'react-bootstrap'


function Arsip() {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
        <Navbar/>
        
        <Container className="py-5">
            <h2>Arsip</h2>

            <Button variant="primary" className="btn btn-success float-right" onClick={handleShow}>
                Unggah dokumen
            </Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Unggah dokumen</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Form>
                <Form.Group controlId="formGroupEmail">
                    <Form.Label>Pengirim</Form.Label>
                    <Form.Control type="#" placeholder="Pengirim" />
                </Form.Group>
                <Form.Group controlId="formGroupEmail">
                    <Form.Label>Perihal</Form.Label>
                    <Form.Control type="#" placeholder="Perihal" />
                </Form.Group>
                <Form.Group controlId="formGroupPassword">
                    <Form.Label>Atasan</Form.Label>
                    <Form.Control type="#" placeholder="Atasan" />
                </Form.Group>
                <Form.Group>
                <Form.File
                    className="position-relative"
                    required
                    name="file"
                    label="Unggah dokumen"
                    
                    
                />
                </Form.Group>
                <Form.Group id="formGridCheckbox">
                    <Form.Check type="checkbox" label="Draft" />
                </Form.Group>
                </Form>
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Batal
                </Button>
                <Button variant="primary" onClick={handleClose}>
                    Submit
                </Button>
                </Modal.Footer>
            </Modal>
            
            <br /><br /><br />
            <Table striped bordered hover responsive>
                <thead>
                    <tr>
                        <th width="4%">#</th>
                        <th width="17%">Kode dokumen</th>
                        <th>Nama dokumen</th>
                        <th width="19%">Status</th>
                        <th width="25%">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>dok01</td>
                        <td>Dokumen 1</td>
                        <td><Badge variant="success">Approved</Badge></td>
                        <td>
                            <a href="/detail-dokumen" className="btn btn-sm btn-outline-primary mx-2" role="button">Lihat detail</a>
                            <a href="#" className="btn btn-sm btn-outline-warning mx-2" role="button">Ubah</a>
                            <a href="#" className="btn btn-sm btn-outline-danger mx-2" role="button">Hapus</a>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>dok02</td>
                        <td>Dokumen 2</td>
                        <td><Badge variant="warning">Terkirim. Menunggu approval</Badge></td>
                        <td>
                            <a href="/detail-dokumen" className="btn btn-sm btn-outline-primary mx-2" role="button">Lihat detail</a>
                            <a href="#" className="btn btn-sm btn-outline-warning mx-2" role="button">Ubah</a>
                            <a href="#" className="btn btn-sm btn-outline-danger mx-2" role="button">Hapus</a>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>dok03</td>
                        <td>Dokumen 3</td>
                        <td><Badge variant="info">Draft</Badge></td>
                        <td>
                            <a href="/detail-dokumen" className="btn btn-sm btn-outline-primary mx-2" role="button">Lihat detail</a>
                            <a href="#" className="btn btn-sm btn-outline-warning mx-2" role="button">Ubah</a>
                            <a href="#" className="btn btn-sm btn-outline-danger mx-2" role="button">Hapus</a>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>dok04</td>
                        <td>Dokumen 4</td>
                        <td><Badge variant="danger">Ditolak</Badge></td>
                        <td>
                            <a href="/detail-dokumen" className="btn btn-sm btn-outline-primary mx-2" role="button">Lihat detail</a>
                            <a href="#" className="btn btn-sm btn-outline-warning mx-2" role="button">Ubah</a>
                            <a href="#" className="btn btn-sm btn-outline-danger mx-2" role="button">Hapus</a>
                        </td>
                    </tr>
                </tbody>
            </Table>
        </Container>
        </>
    )
}

export default Arsip
