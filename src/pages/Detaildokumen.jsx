import React from 'react'
import Navbar from './Navbar';
import { Container, Button, Alert, Figure, Row, Col, Form, Nav, Badge, Table, Card} from 'react-bootstrap'
import doc1 from '../img/doc1.jpg';
function Detaildokumen() {
    return (
        <>
        <Navbar/>
        <Container className="py-5">
            <h2>Detail dokumen</h2>
            <br />
            <Container className="px-0 pb-5">
                <Row>
                    <Col sm={8} className="mb-4">
                        <Card>
                            <Alert variant="light" style={{ marginBottom:"0px" }}>
                                <Nav variant="tabs" defaultActiveKey="#">
                                    <Nav.Item>
                                        <Nav.Link href="#">Preview</Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link href="##">Download</Nav.Link>
                                    </Nav.Item>
                                </Nav>
                                <br />
                                <Figure>
                                    <Figure.Image src={doc1} />
                                    <Figure.Caption>
                                        Preview Document
                                    </Figure.Caption>
                                </Figure>
                            </Alert>
                        </Card>
                    </Col>
                    <Col sm={4}>
                        <Card>
                            <Alert variant="light" style={{ marginBottom:"0px" }}>
                                <h5>Status</h5>
                                <hr />
                                <Badge variant="success">Approved</Badge>
                                <br></br><br></br>
                                <h5>Detail</h5>
                                <hr />
                                <Table borderless="true">
                                    <tbody>
                                        <tr>
                                            <td><b>Pengirim</b></td>
                                            <td>: User1</td>
                                        </tr>
                                        <tr>
                                            <td><b>Perihal</b></td>
                                            <td>: Cuti</td>
                                        </tr>
                                        <tr>
                                            <td><b>Atasan</b></td>
                                            <td>: User2</td>
                                        </tr>
                                        <tr>
                                            <td><b>Tanggal diajukan</b></td>
                                            <td>: 09 Nov 2020</td>
                                        </tr>
                                        <tr>
                                            <td><b>Tanggal disetujui</b></td>
                                            <td>: 10 Nov 2020</td>
                                        </tr>
                                        <tr>
                                            <td><b>Tanggal ditolak</b></td>
                                            <td>: -</td>
                                        </tr>
                                        <tr>
                                            <td><b>Alasan ditolak</b></td>
                                            <td>: -</td>
                                        </tr>

                                    </tbody>

                                </Table>
                            </Alert>
                        </Card>
                        <br />
                        <Card>
                            <Alert variant="light" style={{ marginBottom:"0px" }}>
                                <h5>Opsi</h5>
                                <hr />
                                <Button variant="success">Approve</Button>{' '}
                                <Button variant="danger">Tolak</Button>{' '}
                                <br /><br />
                                <Form>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Alasan Ditolak</Form.Label>
                                        <Form.Control type="email" />

                                    </Form.Group>

                                    <Button variant="primary" type="submit">
                                        Submit
                                    </Button>
                                </Form>
                            </Alert>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </Container>
        </>
    )
}

export default Detaildokumen