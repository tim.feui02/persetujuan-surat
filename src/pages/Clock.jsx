import React from 'react'

class Clock extends React.Component {
    constructor() {
        super()
        this.state = {
            waktu: new Date()
        }
    }

    tick() {
        this.setState({
            waktu: new Date()
        })
    }
    componentDidMount() {
        this.timer = setInterval(
            () => this.tick(), 1000
        )
    }
    componentWillUnmount() {
        clearInterval(this.timer)
    }
    render() {
        return (
            <div>
                <h2>{this.state.waktu.toLocaleTimeString()}</h2>
            </div>
        )
    }
}

export default Clock